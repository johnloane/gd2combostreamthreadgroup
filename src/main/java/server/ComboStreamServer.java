package server;

import core.ComboServiceDetails;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ComboStreamServer {
    public static void main(String[] args) {
        try{
            //Create a listening socket
            ServerSocket listenSocket = new ServerSocket(ComboServiceDetails.SERVERPORT);

            //Create a ThreadGroup to store all clients together
            ThreadGroup group = new ThreadGroup("Client threads");
            //Place more emphasis on accepting threads than on processing threads
            group.setMaxPriority(Thread.currentThread().getPriority()-1);

            //Do main logic of the server
            boolean continueRunning = true;
            int threadCount = 0;

            while(continueRunning){
                //Wait for incoming connections
                Socket dataSocket = listenSocket.accept();

                threadCount++;

                //Build the thread. Thread should be given:
                //1. A group that it is stored
                //2. A name
                //3. A socket to communicate that
                //4. Other useful info

                ComboServiceThread newClient = new ComboServiceThread(group, dataSocket.getInetAddress()+"", dataSocket, threadCount);

                newClient.start();
            }
            listenSocket.close();
        }catch(IOException io){
            io.printStackTrace();
        }
    }
}
