package server;

import core.ComboServiceDetails;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Date;
import java.util.Scanner;

public class ComboServiceThread extends Thread {
    //Create some variables to hold Socket streams, number of thread
    private Socket dataSocket;
    private Scanner input;
    private PrintWriter output;
    private int number;

    public ComboServiceThread(ThreadGroup group, String name, Socket dataSocket, int number){
        super(group, name);

        try{
            this.dataSocket = dataSocket;
            this.number = number;
            input = new Scanner(new InputStreamReader(this.dataSocket.getInputStream()));
            output = new PrintWriter(this.dataSocket.getOutputStream(), true);
        }catch(IOException io){
            io.printStackTrace();
        }
    }

    @Override
    public void run(){
        //Set up variables for communication
        String incomingMessage = "";
        String response;

        try{
            //while the client doesn't want to end the session
            while(!incomingMessage.equals(ComboServiceDetails.ENDSESSION)){
                response = null;
                //Take the input from the client
                incomingMessage = input.nextLine();
                System.out.println("Received message: " + incomingMessage);

                //Tokenize the input
                String[] components = incomingMessage.split(ComboServiceDetails.BREAKINGCHARACTER);

                //Process the information from the client
                if(components[0].equals(ComboServiceDetails.ECHO)){
                    StringBuffer echoMessage = new StringBuffer("");

                    if(components.length > 1){
                        echoMessage.append(components[1]);
                    }
                    response = echoMessage.toString();
                }
                else if(components[0].equals(ComboServiceDetails.DAYTIME)){
                    response = new Date().toString();
                }
                else if(components[0].equals(ComboServiceDetails.ENDSESSION)){
                    response = ComboServiceDetails.SESSIONTERMINATED;

                }
                else{
                    response = ComboServiceDetails.UNRECOGNISED;
                }

                output.println(response);
            }
        }catch(Exception e){
            e.printStackTrace();
        }finally{
            try {
                dataSocket.close();
            }catch(IOException io){
                io.printStackTrace();
            }
        }
    }
}
