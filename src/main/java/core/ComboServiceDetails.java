package core;

public class ComboServiceDetails {

    public static final int SERVERPORT = 50000;

    public static final String BREAKINGCHARACTER = "%%";

    //Command strings
    public static final String ENDSESSION = "QUIT";
    public static final String ECHO = "ECHO";
    public static final String DAYTIME = "DAYTIME";

    //Response Strings
    public static final String UNRECOGNISED = "UNKNOWNCOMMAND";
    public static final String SESSIONTERMINATED = "GOODBYE";

}
